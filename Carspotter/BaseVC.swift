//
//  BaseVC.swift
//  Carspotter
//
//  Created by Michał Tubis on 09.05.2016.
//  Copyright © 2016 Michał Tubis. All rights reserved.
//

import UIKit
import MapKit
import Toast_Swift

class BaseVC: UIViewController, UITextFieldDelegate, CLLocationManagerDelegate {
    
    let spinner = UIActivityIndicatorView()
    let locationManager = CLLocationManager()
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func textFieldFactory(_ margin: CGFloat, field: UITextField, placeholder: String) {
        field.autocorrectionType = .no
        field.leftView = UIView(frame: CGRect(x: 0, y: 0, width: margin, height: 44))
        field.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 44))
        field.leftViewMode = UITextFieldViewMode.always
        field.rightViewMode = UITextFieldViewMode.always
        let tintColor = UIColor(red: 60/255.0, green: 70/255.0, blue: 75/255.0, alpha: 1.0)
        let placeholderColor = UIColor(red: 53/255.0, green: 63/255.0, blue: 68/255.0, alpha: 0.3)
        field.tintColor = tintColor
        field.attributedPlaceholder = NSAttributedString(string:placeholder, attributes:[NSForegroundColorAttributeName: placeholderColor])
    }

    //MARK: Keyboard
    
    func dismissAnyVisibleKeyboards(_ textField: UITextField) -> Bool {
        if textField.isFirstResponder {
            view.endEditing(true)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: Alerts
    
    func showAlert(_ error: String) {
        DispatchQueue.main.async(execute: {
            let alert = UIAlertController(title: "", message: error, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        })
    }
    
    func showOKCancelAlert(_ title: String, message: String, completionHandler: @escaping ((_ success: Bool) -> Void)) {
        DispatchQueue.main.async(execute: {
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                completionHandler(false)
            }))
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                completionHandler(true)
            }))
            self.present(alert, animated: true, completion: nil)
        })
    }

    func showFieldAlert(_ title: String, message: String, secure: Bool, completionHandler: @escaping ((_ string: String, _ success: Bool) -> Void)) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addTextField(configurationHandler: { (textField) -> Void in
            textField.text = ""
            textField.isSecureTextEntry = secure
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action) -> Void in
            completionHandler("", false)
        }))
        alert.addAction(UIAlertAction(title: "OK", style: .default , handler: { (action) -> Void in
            let alertField = alert.textFields![0] as UITextField
            completionHandler(alertField.text!, true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: Image Conversion
    
    func convertImageToBase64(_ image: UIImage) -> String {
        
        let imageData = UIImagePNGRepresentation(image)
        let base64String = imageData!.base64EncodedString(options: Data.Base64EncodingOptions([]))
        return base64String
    }
    
    func convertBase64ToImage(_ base64String: String) -> UIImage {
        
        let decodedData = Data(base64Encoded: base64String, options: Data.Base64DecodingOptions(rawValue: 0) )
        
        let decodedimage = UIImage(data: decodedData!)
        
        return decodedimage!
    }
    
    func imageResize (_ image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 3.5 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return scaledImage!
    }
    
    func resizeImage(_ imageString: String) -> UIImage {
        
        let image = self.convertBase64ToImage(imageString)
        var square = CGSize(width: 1000, height: 1000)
        let size = CGSize(width: 40, height: 40)
        
        if image.size.width > 1280 {
            square = CGSize(width: 4000, height: 4000)
        }
        let squareImage = image.imageByCroppingImage(square)
        let resizedImage = self.imageResize(squareImage, sizeChange: size)
        
        return resizedImage
    }
    
    
    // MARK: Helpers
    
    func concatenateStrings (_ left: NSAttributedString, right: NSAttributedString) -> NSAttributedString
    {
        let result = NSMutableAttributedString()
        result.append(left)
        result.append(right)
        return result
    }
}

extension UIImage
{
    func roundImage() -> UIImage
    {
        let newImage = self.copy() as! UIImage
        let cornerRadius = self.size.height/2
        
        UIGraphicsBeginImageContextWithOptions(self.size, false, 0.0)
        let bounds = CGRect(origin: CGPoint.zero, size: self.size)
        UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).addClip()
        newImage.draw(in: bounds)
        let finalImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return finalImage!
    }
    
    func imageByCroppingImage(_ size : CGSize) -> UIImage
    {
        let refWidth : CGFloat = CGFloat(self.cgImage!.width)
        let refHeight : CGFloat = CGFloat(self.cgImage!.height)
        
        let x = (refWidth - size.width) / 2
        let y = (refHeight - size.height) / 2
        
        let cropRect = CGRect(x: x, y: y, width: size.height, height: size.width)
        let imageRef = self.cgImage!.cropping(to: cropRect)
        
        let cropped : UIImage = UIImage(cgImage: imageRef!, scale: 0, orientation: self.imageOrientation)
        
        
        return cropped
    }
}
