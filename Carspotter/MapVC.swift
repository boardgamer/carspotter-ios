//
//  MapVC.swift
//  Carspotter
//
//  Created by Michał Tubis on 15.04.2016.
//  Copyright © 2016 Michał Tubis. All rights reserved.
//

import UIKit
import MapKit
import Firebase
import JSQSystemSoundPlayer
import Crashlytics
import INTULocationManager

class MapVC: BaseVC, MKMapViewDelegate, UINavigationControllerDelegate, UIViewControllerTransitioningDelegate {
    
    // MARK: - Properties
    
    var users = [User]()
    var annotations = [MKPointAnnotation]()
//    var longitude : Double?
//    var latitude : Double?
    
    let calloutButton = UIButton(type: .detailDisclosure) as UIButton

    @IBOutlet var mapView: MKMapView!
    @IBOutlet var button: UIButton!
    
    
    // MARK: - View Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Answers.logContentView(withName: "MapVC",
                                       contentType: "",
                                       contentId: "",
                                       customAttributes: [:])
        
        mapView.delegate = self
        mapView.showsCompass = false
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
        locationManager.requestWhenInUseAuthorization()
        updateLocation()
        
        button.isHidden = true
        button.layer.cornerRadius = 3
        spinner.hidesWhenStopped = true
        spinner.center = view.center
        spinner.color = UIColor.black
        view.addSubview(spinner)
        
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont.systemFont(ofSize: 17, weight: UIFontWeightMedium)]
        navigationController?.navigationBar.titleTextAttributes = titleDict as? [String : AnyObject]
        navigationItem.backBarButtonItem = UIBarButtonItem(title:" ", style:.plain, target:nil, action:nil)
        
        navigationController!.navigationBar.barStyle = UIBarStyle.black
        navigationController!.navigationBar.tintColor = UIColor.white
        self.navigationController?.isNavigationBarHidden = true
        
        //TODO: Only download necessary info at startup
        // Sort out notification update
        spinner.startAnimating()
        FirebaseClient.sharedInstance().getUserData { (success, notifications) in
            self.updateNotifications(notifications)
            FirebaseClient.sharedInstance().getUserLoaction()
            self.adjustButton()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    // MARK: - UI Adjustment
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    func adjustButton() {
        generatePins { (success) in
            
            if User.UserData.latitude == nil || User.UserData.longitude == nil {
                self.button.setTitle("Check in", for: UIControlState())
                self.button.backgroundColor = UIColor(red: 211/255, green: 111/255, blue: 5/255, alpha: 1.0)
                self.button.isHidden = false
            } else {
                self.button.setTitle("Check out", for: UIControlState())
                self.button.backgroundColor = UIColor(red: 53/255, green: 63/255, blue: 68/255, alpha: 1.0)
                self.button.isHidden = false
            }
        }
    }
    
    // MARK: - API
    
    func generatePins(_ completionHandler: @escaping (_ success: Bool) -> Void) {
        
                spinner.startAnimating()
                FirebaseClient.Constants.LocationsURL.observe(.value, with: { snapshot in
                    if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
                        for snap in snapshots {
                            if let _ = snap.value as? Dictionary<String, AnyObject> {
                                if (snap.value as AnyObject).object(forKey: "latitude") != nil && (snap.value as AnyObject).object(forKey: "longitude") != nil{
                                    self.mapView.removeAnnotations(self.annotations)
                                    self.annotations.removeAll()
                                    
                                    let lat = CLLocationDegrees((snap.value as AnyObject).object(forKey: "latitude")! as! Double)
                                    let long = CLLocationDegrees((snap.value as AnyObject).object(forKey: "longitude")! as! Double)
                                    let uid = ((snap.value as AnyObject).object(forKey: "uid")!)
                                    let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
                                    let blockedUsers = ((snap.value as AnyObject).object(forKey: "blockedUsers")) as? [String: AnyObject]
                                    
                                    let annotation = CustomPointAnnotation()
                                    annotation.uid = uid as! String
                                    annotation.latitude = lat
                                    annotation.longitude = long
                                    annotation.coordinate = coordinate
                                    annotation.blockedUsers = blockedUsers
                                    annotation.title = "Loading..."

                                    DispatchQueue.main.async() {
                                        
                                        self.annotations.append(annotation)
                                        self.mapView.addAnnotations(self.annotations)
                                        FirebaseClient.Constants.LocationsURL.removeAllObservers()
                                        self.spinner.stopAnimating()
                                    }
                                }
                            }
                        }
                        self.spinner.stopAnimating()
                        completionHandler(false)
                    } else {
                        self.spinner.stopAnimating()
                        completionHandler(false)
                    }
                })
    }
    
    @IBAction func refreshMap(_ sender: AnyObject) {
        
        let connectedRef = FirebaseClient.Constants.BaseURL.child(".info/connected")
        connectedRef.observe(.value, with: { (connected) in
            
            if let boolean = connected.value as? Bool, boolean == true {
                self.generatePins { (success) in }
            } else {
                self.view.makeToast("Connection error")
            }
            connectedRef.removeAllObservers()
        })
    }
    
    func sendLocation(_ checkin: Bool, latitude: Double?, longitude: Double?) {
        
        // Set uid
        let uid = UserDefaults.standard.value(forKey: "uid") as! String
        var uidData: AnyObject? = .none
        if checkin == true {
            uidData = uid as AnyObject?
        } else {
            uidData = nil
        }
        
        // Set latitude
        let latitudeReference = FirebaseClient.Constants.LocationsURL.child(uid).child("latitude")
        FirebaseClient.sharedInstance().sendData(latitudeReference, data: latitude as AnyObject?, completionHandler: { (success) in
            if success == false {
                self.spinner.stopAnimating()
            }
        })
        // Set longitude
        let longitudeReference = FirebaseClient.Constants.LocationsURL.child(uid).child("longitude")
        FirebaseClient.sharedInstance().sendData(longitudeReference, data: longitude as AnyObject?, completionHandler: { (success) in
            self.didFinishChecking(checkin, success: success, latitude: latitude as AnyObject?, longitude: longitude as AnyObject?)
        })
        let uidReference = FirebaseClient.Constants.LocationsURL.child(uid).child("uid")
        FirebaseClient.sharedInstance().sendData(uidReference, data: uidData, completionHandler: { (success) in
        })
    }

    //  MARK: - Checking in/out
    
    func checkIn() {
        spinner.startAnimating()
        let locMgr: INTULocationManager = INTULocationManager.sharedInstance()
        locMgr.requestLocation(withDesiredAccuracy: INTULocationAccuracy.block,
                               timeout: 10.0,
                               delayUntilAuthorized: true,
                               block: {(currentLocation: CLLocation?, achievedAccuracy: INTULocationAccuracy, status: INTULocationStatus) -> Void in
                                if status.rawValue == 0 || status.rawValue == 1 {
                                    print("got location")
                                    let latitude = self.locationManager.location?.coordinate.latitude
                                    let longitude = self.locationManager.location?.coordinate.longitude
                                    self.sendLocation(true, latitude: latitude, longitude: longitude)
                                    self.setRegionAnimated(latitude: latitude!, longitude: longitude!)
                                    self.spinner.stopAnimating()
                                }
                                else {
                                    print("no location")
                                    print(status.rawValue)
                                    self.processGeolocationErrors(status: status.rawValue)
                                    self.spinner.stopAnimating()
                                }
        })
    }
    
    func checkOut() {
        
        sendLocation(false, latitude: nil, longitude: nil)
    }
    
    func didFinishChecking(_ checkin: Bool, success: Bool, latitude: AnyObject?, longitude: AnyObject?){
        if success {
            if checkin == false {
                User.UserData.latitude = nil
                User.UserData.longitude = nil
            } else {
                User.UserData.latitude = latitude as! Double?
                User.UserData.longitude = longitude as! Double?
            }
            self.adjustButton()
        } else {
            self.view.makeToast("Connection error")
            self.spinner.stopAnimating()
        }
    }
    
    func showPrivacyAlert(_ error: String, completionHandler: @escaping (( _ success: Bool) -> Void)) {
        DispatchQueue.main.async(execute: {
            let alert = UIAlertController(title: "Show me on the map", message: error, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                completionHandler(false)
            }))
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                completionHandler(true)
            }))
            self.present(alert, animated: true, completion: nil)
        })
    }
    
    @IBAction func buttonTapped(_ sender: AnyObject) {
        
        if User.UserData.latitude == nil {
            if User.UserData.privacyAgreement != nil {
                checkIn()
            } else {
                showOKCancelAlert("Show me on the map", message: "This will show your location on the map for every user of the app until you tap to check out. Do you want to continue?", completionHandler: { (success) in
                    if success == true {
                        self.spinner.startAnimating()
                        let uid = UserDefaults.standard.value(forKey: "uid") as! String
                        let privacyAgreement = FirebaseClient.Constants.UsersURL.child(uid).child("privacyAgreement")
                        
                        FirebaseClient.sharedInstance().sendData(privacyAgreement, data: true as AnyObject?, completionHandler: { (success) in
                            if success {
                                self.checkIn()
                                User.UserData.privacyAgreement = true
                            } else {
                                self.view.makeToast("Connection error")
                                self.spinner.stopAnimating()
                            }
                        })
                    }
                })
            }
        } else {
            checkOut()
        }
    }
    
    // MARK: - Geolocation
    
    func updateLocation() {
        let locMgr: INTULocationManager = INTULocationManager.sharedInstance()
        locMgr.requestLocation(withDesiredAccuracy: INTULocationAccuracy.block,
                               timeout: 10.0,
                               delayUntilAuthorized: true,
                               block: {(currentLocation: CLLocation?, achievedAccuracy: INTULocationAccuracy, status: INTULocationStatus) -> Void in
                                if status.rawValue == 0 || status.rawValue == 1 {
                                    print("got location")
                                    let latitude = self.locationManager.location?.coordinate.latitude
                                    let longitude = self.locationManager.location?.coordinate.longitude
                                    self.setRegionAnimated(latitude: latitude!, longitude: longitude!)
                                }
                                else {
                                    print("no location")
                                    print(status.rawValue)
                                }
        })
    }
    
    func processGeolocationErrors(status: Int) {
        if status == 2 || status == 3  || status == 5 {
            self.showOKCancelAlert("Turn on location services", message: "Do you want to allow Carspotter to access your device's location?", completionHandler: { (success) in
                if success == true {
                    UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
                    //TODO: Take to main Settings when Location Services switched off for all apps
                }
            })
        }
        if status == 4 {
            showAlert("Location services are restricted for your device")
            print("Parent restrictions")
        }
        if status == 6 {
            self.view.makeToast("Location services error")
            print("Error")
        }
    }
    
    // MARK: - Pins & Annotations
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        let reuseId = "Place"
        var anView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: "pin")
            anView!.canShowCallout = true
            calloutButton.tintColor = UIColor(red: 211/255, green: 111/255, blue: 5/255, alpha: 1.0)
            anView!.rightCalloutAccessoryView = calloutButton
            calloutButton.isEnabled = false
        } else {
            anView!.annotation = annotation
        }
        
        let cpa = annotation as! CustomPointAnnotation
        let uid = UserDefaults.standard.value(forKey: "uid") as! String
        
        if cpa.blockedUsers != nil {
            for (_, user) in cpa.blockedUsers! {
                if user as! String == uid {
                    cpa.blocked = true
                }
            }
        }
        
        if (cpa.uid)! == uid {
            if User.UserData.base64String != nil {
                createImagePin(User.UserData.base64String!, anView: anView!)
            } else {
                FirebaseClient.sharedInstance().getUserImage(cpa.uid, completionHandler: { (success, imageString) in
                    if success {
                        User.UserData.base64String = imageString
                        self.createImagePin(imageString, anView: anView!)
                    }
                })
            }
        } else if cpa.blocked == true {
            anView!.image = nil
            anView!.canShowCallout = false
        } else {
            anView!.layer.cornerRadius = 1
            anView!.layer.borderWidth = 0
            anView!.image = UIImage(named: "pin")
            anView!.canShowCallout = true
            anView!.backgroundColor = UIColor.clear
        }
        return anView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView)
    {
        
        let cpa = view.annotation as! CustomPointAnnotation
        var imageView = UIImageView()
        imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        imageView.clipsToBounds = true
        imageView.autoresizingMask = UIViewAutoresizing.flexibleRightMargin
        if cpa.imageString == nil {
            imageView.image = UIImage(named: "photo_placeholder")
        } else {
            imageView.image = self.resizeImage(cpa.imageString).roundImage()
        }
        imageView.layer.cornerRadius = imageView.layer.frame.size.width / 2
        view.leftCalloutAccessoryView = imageView
        
        // Annotation label
        let aboutMe = UILabel()
        let views = ["aboutMeView": aboutMe]
        aboutMe.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[aboutMeView(180)]", options: [], metrics: nil, views: views))
        aboutMe.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[aboutMeView(30)]", options: [], metrics: nil, views: views))
        if cpa.about != nil {
            aboutMe.text = cpa.about
        }
        aboutMe.numberOfLines = 2
        aboutMe.font = UIFont.systemFont(ofSize: 12, weight: UIFontWeightLight)
        view.detailCalloutAccessoryView = aboutMe
        
        FirebaseClient.sharedInstance().getPublicUserData(cpa.uid) { (result, success) in
            
            if success {
                cpa.title = (result["displayName"] as? String)!
                cpa.about = (result["about"] as? String)!
                cpa.blockedUsers = (result["blockedUsers"] as? [String: AnyObject])
                aboutMe.text = cpa.about
                
                let uid = UserDefaults.standard.value(forKey: "uid") as! String
                
//                if cpa.blockedUsers != nil {
//                    for (_, user) in cpa.blockedUsers! {
//                        if user as! String == uid {
//                            cpa.blocked = true
//                        }
//                    }
//                }
                
                if cpa.imageString == nil {
                    imageView.image = UIImage(named: "photo_placeholder")
                    FirebaseClient.sharedInstance().getUserImage(cpa.uid, completionHandler: { (success, imageString) in
                        
                        cpa.imageString = imageString
                        imageView.image = self.resizeImage(imageString).roundImage()
                        
                        self.calloutButton.isEnabled = true
                    })
                } else {
                    self.calloutButton.isEnabled = true
                }
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        let cpa = view.annotation as! CustomPointAnnotation
        if cpa.imageString != nil {
            if control == view.rightCalloutAccessoryView {
                
                let userProfile = self.storyboard!.instantiateViewController(withIdentifier: "UserProfileVC") as! UserProfileVC
                
                userProfile.displayName = cpa.title!
                userProfile.latitude = cpa.latitude
                userProfile.longitude = cpa.longitude
                userProfile.about = cpa.about!
                userProfile.imageString = cpa.imageString
                userProfile.uid = cpa.uid
                userProfile.showMessage = true
                
                navigationController!.pushViewController(userProfile, animated: true)
            }
        }
    }
    
    // MARK: - Map
    
    func setRegionAnimated(latitude: Double, longitude: Double) {
        let annotation = MKPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let span = MKCoordinateSpanMake(1, 1)
        let region = MKCoordinateRegionMake(annotation.coordinate, span)
        self.mapView.setRegion(region, animated: true)
    }
    
    // MARK: - Helper methods
    
    func createImagePin(_ imageString: String, anView: MKAnnotationView){
        
        anView.image = resizeImage(imageString).roundImage()
        anView.layer.cornerRadius = anView.frame.size.height/2
        anView.layer.borderColor = UIColor.white.cgColor
        anView.layer.borderWidth = 3
        anView.layer.shadowOffset = CGSize(width: 0, height: 6)
        anView.layer.shadowOpacity = 0.4
        anView.layer.shadowRadius = 2
        anView.canShowCallout = true
        anView.backgroundColor = UIColor.clear
    }


    func updateNotifications(_ notifications: Int) {
        let tabArray = self.tabBarController?.tabBar.items as NSArray!
        let tabItem = tabArray?.object(at: 1) as! UITabBarItem
        UIApplication.shared.applicationIconBadgeNumber = notifications
        if notifications != 0 {
            tabItem.badgeValue = "\(notifications)"
        } else {
            tabItem.badgeValue = nil
        }
    }
}
