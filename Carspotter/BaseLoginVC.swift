//
//  BaseLoginVC.swift
//  Carspotter
//
//  Created by Michał Tubis on 18.04.2016.
//  Copyright © 2016 Michał Tubis. All rights reserved.
//

import UIKit

class BaseLoginVC: BaseVC {
    
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var baseButton: UIButton!
    @IBOutlet var fbButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        spinner.hidesWhenStopped = true
        spinner.center = self.view.center
        spinner.color = UIColor.black
        view.addSubview(spinner)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: Base Actions
    
    func textFieldFactory (_ field:UITextField, name:String, frame: CGRect) {
        
        field.autocorrectionType = .no
        let fieldColor = UIColor (red: 240/255.0, green: 124/255.0, blue: 5/255.0, alpha: 0.6)
        field.attributedPlaceholder = NSAttributedString(string:name, attributes:[NSForegroundColorAttributeName: UIColor.white])
        field.backgroundColor = fieldColor
        field.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: field.frame.size.height))
        field.rightView = UIView(frame: CGRect(x: 0, y: 0, width: field.frame.size.width-235, height: field.frame.size.height))
        field.layer.cornerRadius = 2
        field.tintColor = UIColor.white
        let imageView = UIImageView()
        imageView.image = UIImage(named:name)
        field.leftViewMode = UITextFieldViewMode.always
        field.rightViewMode = UITextFieldViewMode.always
        imageView.frame = frame
        field.addSubview(imageView)
    }
    
    func buttonFactory(_ button: UIButton, label:String) {
        if label == "" {
            button.backgroundColor = UIColor(red: 59/255.0, green: 89/255.0, blue: 152/255.0, alpha: 1.0)
        } else {
            button.backgroundColor = UIColor(red: 53/255.0, green: 63/255.0, blue: 68/255.0, alpha: 1.0)
        }
        button.layer.cornerRadius = 2
        button.setTitle(label, for: UIControlState())
        button.setTitleColor(UIColor.white, for: UIControlState())
        button.titleLabel!.font = UIFont.boldSystemFont(ofSize: 18.0)
    }
    
    func showView(_ view: String) {
        DispatchQueue.main.async(execute: {
            let controller = self.storyboard!.instantiateViewController(withIdentifier: view)
            self.spinner.stopAnimating()
            controller.modalTransitionStyle = UIModalTransitionStyle.flipHorizontal
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @IBAction func backToLogin(_ sender: AnyObject) {
        self.dismiss(animated: true) {}
    }
    
}
