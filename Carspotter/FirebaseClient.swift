//
//  FirebaseClient.swift
//  Carspotter
//
//  Created by Michał Tubis on 15.04.2016.
//  Copyright © 2016 Michał Tubis. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FBSDKLoginKit
import FBSDKCoreKit
import OneSignal

class FirebaseClient : BaseVC {
    
    var plistFileName = Bundle.main.infoDictionary?["TargetName"]
    
    // MARK: - Shared Instance
    
    class func sharedInstance() -> FirebaseClient {
        struct Singleton {
            static var sharedInstance = FirebaseClient()
        }
        return Singleton.sharedInstance
    }
    
    func authErrorHandler (_ error: NSError, completionHandler: ((_ success: Bool, _ message: String) -> Void)) {
        
        if let errorCode = FIRAuthErrorCode(rawValue: error.code) {
            switch (errorCode) {
            case .errorCodeInvalidEmail:
                completionHandler(false, "The specified email is not valid")
            case .errorCodeWrongPassword:
                completionHandler(false, "Incorrect Password")
            case .errorCodeUserNotFound:
                completionHandler(false, "This user account does not exist")
            case .errorCodeNetworkError:
                completionHandler(false, "An error occurred while attempting to contact our server.")
            case .errorCodeEmailAlreadyInUse:
                completionHandler(false, "This email address is already in use")
            case .errorCodeRequiresRecentLogin:
                completionHandler(false, "Relogin required")
            default:
                completionHandler(false, "Something went wrong :(")
                print(errorCode)
            }
        }
    }
    
    func createUser(_ name: String, email: String, password: String, completionHandler: @escaping ((_ success: Bool, _ message: String) -> Void)){
        
        FIRAuth.auth()?.createUser(withEmail: email, password: password) { (user, error) in
            if error != nil {
                self.authErrorHandler(error as! NSError, completionHandler: { (success, message) in
                    completionHandler(success, message)})
            } else {
                let newUser: Dictionary<String, AnyObject> = [
                    "firstName": name as AnyObject,
                    "email": email as AnyObject,
                    "about": "" as AnyObject,
                    "oneSignalID": "" as AnyObject,
                    "base64String": self.convertImageToBase64(UIImage(named:"photo_placeholder")!) as AnyObject,
                    "provider": "password" as AnyObject,
                    "uid": user!.uid as AnyObject
                ]
                FirebaseClient.Constants.UsersURL.child((user?.uid)!).setValue(newUser)
                
                UserDefaults.standard.setValue(user?.uid, forKey: "uid")
                completionHandler(true, "Successfully authenticated user!")
                print("Successfully created user account!")
            }
        }
    }
    
    func logInUser(_ email: String, password: String, completionHandler: @escaping ((_ success: Bool, _ message: String) -> Void)) {
        
        FIRAuth.auth()?.signIn(withEmail: email, password: password) { (user, error) in
            if error != nil {
                self.authErrorHandler(error as! NSError, completionHandler: { (success, message) in
                    completionHandler(success, message)})
            } else {
                UserDefaults.standard.setValue(user?.uid, forKey: "uid")
                completionHandler(true, "Successfully authenticated user!")
            }
        }
    }
    
    func fbLogIn (_ completionHandler: @escaping ((_ success: Bool, _ message: String) -> Void)) {
        
        let facebookLogin = FBSDKLoginManager()
        
        facebookLogin.logIn(withReadPermissions: ["email"], from: nil, handler:{(facebookResult, facebookError) -> Void in
            if facebookError != nil {
                completionHandler(false, "Facebook login failed")
                
            } else if (facebookResult?.isCancelled)! {
                completionHandler(false, "Cancelled")
                
            } else {
                let credential = FIRFacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
                
                FIRAuth.auth()?.signIn(with: credential) { (user, error) in

                    if error != nil {
                        completionHandler(false, "Login failed!")
                    } else {
                        let userData = FirebaseClient.Constants.UsersURL.child((user?.uid)!)
                        
                        userData.observe(.value, with: { snap in
                            
                            if snap.value is NSNull {
                                
                                print(user?.providerData as Any)
                                
                                let fbImageString = user?.photoURL?.absoluteString
                                let imageURL = NSURL(string: fbImageString!)
                                let data: NSData = NSData(contentsOf: imageURL! as URL)!
                                let fbImage = UIImage(data: data as Data)
                                
                                let newUser: Dictionary <String, AnyObject> = [
                                    "firstName": user!.displayName as AnyObject,
                                    "email": user!.email as AnyObject,
                                    "about": "" as AnyObject,
                                    "oneSignalID": "" as AnyObject,
                                    "base64String": (self.convertImageToBase64(fbImage!) as AnyObject),
                                    "provider": "facebook" as AnyObject,
                                    "uid": (user!.uid as AnyObject)
                                ]
                                FirebaseClient.Constants.UsersURL.child((user?.uid)!).setValue(newUser)
                                print("creating new user")
                                UserDefaults.standard.setValue(user?.uid, forKey: "uid")
                                completionHandler(true, "Successfully authenticated user!")
                            } else {
                                print("Existing user")
                                UserDefaults.standard.setValue(user?.uid, forKey: "uid")
                                completionHandler(true, "Successfully authenticated user!")
                            }
                            userData.removeAllObservers()
                        })
                    }
                    
                    }
            }
        });
    }
    
    func fbLogout() {
        let loginManager: FBSDKLoginManager = FBSDKLoginManager()
        loginManager.logOut()
    }
    
    func getUserLoaction(){
        
        print("getting user location...")
        
        let uid = UserDefaults.standard.value(forKey: "uid") as! String
        
        FirebaseClient.Constants.LocationsURL.child(uid).observe(.value, with: {
            snapshot in
            
            if !(snapshot.value is NSNull) {
                if let latitude = (snapshot.value as AnyObject).object(forKey: "latitude") as? Double {
                    User.UserData.latitude = latitude
                }
                if let longitude = (snapshot.value as AnyObject).object(forKey: "longitude") as? Double {
                    User.UserData.longitude = longitude
                }
            }
            
            FirebaseClient.Constants.LocationsURL.child(uid).removeAllObservers()
            print("finished getting user location")
        })
    }
    
    func getUserImage(_ uid: String, completionHandler: @escaping ((_ success: Bool, _ imageString: String) -> Void)) {
        print("getting user image...")
        
        FirebaseClient.Constants.UsersURL.child(uid).child("base64String").observe(.value, with: {
            snapshot in
            
            let imageString = snapshot.value as? String
            completionHandler(true, imageString!)
            
            FirebaseClient.Constants.UsersURL.child(uid).removeAllObservers()
            print("finished getting user image")
        })
    }
    
    func updateNotifications(_ uid: String, completionHandler: @escaping (_ result: Int) -> Void) {
        var notifications = [Bool]()
        FirebaseClient.Constants.UsersURL.child(uid).child("messages").observe(.value, with: { snapshot in
            if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
                notifications.removeAll()
                for snap in snapshots {
                    if let _ = snap.value as? Dictionary<String, AnyObject> {
                        
                        if let new = (snap.value as AnyObject).object(forKey: "new") as? Bool {
                            if new == true {
                                notifications.append(new)
                            }
                        }
                    }
                }
                let result = notifications.count
                completionHandler(result)
            }
//            FirebaseClient.Constants.UsersURL.child(uid).child("messages").removeAllObservers()
        })
    }
    
    func getUserData(_ completionHandler: @escaping ((_ success: Bool, _ notifications: Int) -> Void)) {
        
        print("getting user data...")
        
        let uid = UserDefaults.standard.value(forKey: "uid") as! String
        
        FirebaseClient.Constants.UsersURL.child(uid).observe(.value, with: {
            snapshot in
            
            if let firstname = (snapshot.value as AnyObject).object(forKey: "firstName") as? String {
                User.UserData.firstName = firstname
            }
            if let email = (snapshot.value as AnyObject).object(forKey: "email") as? String {
                User.UserData.email = email
            }
//            if let latitude = snapshot.value.objectForKey("latitude") as? Double {
//                User.UserData.latitude = latitude
//            }
//            if let longitude = snapshot.value.objectForKey("longitude") as? Double {
//                User.UserData.longitude = longitude
//            }
            
            if let about = (snapshot.value as AnyObject).object(forKey: "about") as? String {
                User.UserData.about = about
            }
            
            if let base64String = (snapshot.value as AnyObject).object(forKey: "base64String") as? String {
                User.UserData.base64String = base64String
            }

            if let provider = (snapshot.value as AnyObject).object(forKey: "provider") as? String {
                User.UserData.provider = provider
            }
            
            if let notifications = (snapshot.value as AnyObject).object(forKey: "notifications") as? Int {
                User.UserData.notifications = notifications
            }
            
            if let oneSignalID = (snapshot.value as AnyObject).object(forKey: "oneSignalID") as? String {
                User.UserData.oneSignalID = oneSignalID
            }
            
            if let privacyAgreement = (snapshot.value as AnyObject).object(forKey: "privacyAgreement") as? Bool {
                User.UserData.privacyAgreement = privacyAgreement
            }
            
            if let blockedUsers = (snapshot.value as AnyObject).object(forKey: "blockedUsers") as? [String: AnyObject] {
                User.UserData.blockedUsers = blockedUsers
            }
            
            self.updateNotifications(uid, completionHandler: { (result) in
                completionHandler(true, result)
            })
            
            let reference = FirebaseClient.Constants.UsersURL.child(uid).child("oneSignalID")

            OneSignal.idsAvailable({ (userId, pushToken) in
                if (pushToken != nil) {
                    reference.setValue(userId)
                }
            })
            print("finished getting user data")
            FirebaseClient.Constants.UsersURL.child(uid).removeAllObservers()
        })
    }
    

    
    func getPublicUserData(_ uid: String, completionHandler: @escaping (_ result: [String: AnyObject], _ success: Bool) -> Void) {
        FirebaseClient.Constants.UsersURL.child(uid).observe(.value, with: {
            snapshot in
            
            let firstname = (snapshot.value as AnyObject).object(forKey: "firstName") as? String
            let about = (snapshot.value as AnyObject).object(forKey: "about") as? String
            let base64String = (snapshot.value as AnyObject).object(forKey: "base64String") as? String
            let blockedUsers = (snapshot.value as AnyObject).object(forKey: "blockedUsers") as? [String: AnyObject]
            let longitude = (snapshot.value as AnyObject).object(forKey: "longitude") as? Double
            let latitude = (snapshot.value as AnyObject).object(forKey: "latitude") as? Double
            
            let userData: [String : AnyObject] = [
                "displayName" : firstname! as AnyObject,
                "about": about! as AnyObject,
                "base64String": base64String! as AnyObject,
                "blockedUsers": blockedUsers as AnyObject,
                "longitude": longitude as AnyObject,
                "latitude": latitude as AnyObject
            ]
            
            if let latitude = (snapshot.value as AnyObject).object(forKey: "latitude") {
//                userData["latitude"] = latitude as AnyObject?
            }
            if let longitude = (snapshot.value as AnyObject).object(forKey: "longitude") as? Double {
//                userData["longitude"] = longitude as AnyObject?
            }
//            if let blockedUsers = (snapshot.value as AnyObject).object(forKey: "blockedUsers") as? [String: AnyObject] {
//                userData["blockedUsers"] = blockedUsers as AnyObject?
//            }
            
            completionHandler(userData, true)
            print("getting data for \(uid)")
            FirebaseClient.Constants.UsersURL.child(uid).removeAllObservers()
        })
        
    }
    
    func changeEmail(_ password: String, oldEmail: String, newEmail: String, completionHandler: @escaping ((_ success: Bool, _ message: String) -> Void)) {
        
        let uid = UserDefaults.standard.value(forKey: "uid") as! String
        
        logInUser(oldEmail, password: password) { (success, message) in
            if success == true {
                FIRAuth.auth()?.currentUser?.updateEmail(newEmail) { (error) in
                    if error != nil {
                        FirebaseClient.sharedInstance().authErrorHandler(error as! NSError, completionHandler: { (success, message) in
                            completionHandler(success, message)})
                    } else {
                        FirebaseClient.Constants.UsersURL.child(uid).child("email").setValue(newEmail)
                        completionHandler(true, "Email address successfully changed!")
                    }
                }
            } else {
                completionHandler(success, message)
            }
        }
    }
    
    func changePassword(_ password: String, newPassword: String, completionHandler: @escaping ((_ success: Bool, _ message: String) -> Void)) {
        
        logInUser(User.UserData.email!, password: password) { (success, message) in
            if success == true {
                FIRAuth.auth()?.currentUser?.updatePassword(newPassword) { (error) in
                    if error != nil {
                        FirebaseClient.sharedInstance().authErrorHandler(error as! NSError, completionHandler: { (success, message) in
                            completionHandler(success, message)})
                    } else {
                        completionHandler(true, "Password successfully changed!")
                    }
                }
            } else {
                completionHandler(success, message)
            }
        }
    }
    
    func resetPassword(_ email: String, completionHandler: @escaping ((_ success: Bool, _ message: String) -> Void)) {
        
        FIRAuth.auth()?.sendPasswordReset(withEmail: email) { (error) in
            if error != nil {
                FirebaseClient.sharedInstance().authErrorHandler(error as! NSError, completionHandler: { (success, message) in
                    completionHandler(success, message)
                })
            } else {
                completionHandler(true, "Password reset email sent successfully!")
            }
        }
    }
    
    func sendData(_ reference: FIRDatabaseReference, data: AnyObject?, completionHandler: @escaping ((_ success: Bool) -> Void)) {
        
        let connectedRef = FirebaseClient.Constants.BaseURL.child(".info/connected")
        connectedRef.observe(.value, with: { (connected) in
            
            if let boolean = connected.value as? Bool, boolean == true {
                reference.setValue(data)
                completionHandler(true)
            } else {
                completionHandler(false)
            }
            connectedRef.removeAllObservers()
        })
    }
    
}

