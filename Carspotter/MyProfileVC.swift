//
//  MyProfileVC.swift
//  Carspotter
//
//  Created by Michał Tubis on 25.04.2016.
//  Copyright © 2016 Michał Tubis. All rights reserved.
//

import UIKit
import CoreData
import OneSignal
import Firebase
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}

import Crashlytics

class MyProfileVC: BaseVC, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource {
    
    let uid = UserDefaults.standard.value(forKey: "uid") as? String
    let imagePicker = UIImagePickerController()
    let savedMessage = "Saved successfully!"
    let errorMessage = "Connection error"
    let appVersion: AnyObject? = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String as AnyObject?
    var email: String? = nil
    var name: String? = nil
    var about: String? = nil
    
    @IBOutlet var photoView: UIView!
    @IBOutlet var photo: UIImageView!
    @IBOutlet var profileBackgrond: UIImageView!
    @IBOutlet var nameField: UITextField!
    @IBOutlet var aboutField: UITextView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var emailField: UITextField!
    @IBOutlet var passwordLabel: UILabel!
    @IBOutlet var versionLabel: UILabel!
    @IBOutlet var profileBackgroundTopConstraint: NSLayoutConstraint!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
//    lazy var sharedContext: NSManagedObjectContext =  {
//        return CoreDataStackManager.sharedInstance().managedObjectContext
//    }()
    
    // MARK: App lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Answers.logContentView(withName: "MyProfileVC",
                                       contentType: "",
                                       contentId: "",
                                       customAttributes: [:])
        
        spinner.hidesWhenStopped = true
        spinner.center = view.center
        spinner.color = UIColor.black
        view.addSubview(spinner)
        
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        nameField.delegate = self
        aboutField.delegate = self
        emailField.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        
        self.spinner.startAnimating()
        FirebaseClient.sharedInstance().getUserData { (success, notifications) in
            if success == true {
                DispatchQueue.main.async {
                self.completeUserData()
                self.completeImage()
                self.makeUI()

                if User.UserData.provider == "facebook" {
                    self.tableView.isHidden = true
                }
                self.spinner.stopAnimating()
                }
            }
        }
        
        versionLabel.text = "v.\(appVersion!)"
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont.systemFont(ofSize: 17, weight: UIFontWeightMedium)]
        navigationController?.navigationBar.titleTextAttributes = titleDict as? [String : AnyObject]
        navigationItem.backBarButtonItem?.tintColor = UIColor.white
        navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)

    }
    
        func getImageWithColor(_ color: UIColor, size: CGSize) -> UIImage {
            UIGraphicsBeginImageContextWithOptions(size, false, 0)
            color.setFill()
            UIRectFill(CGRect(x: 0, y: 0, width: size.width, height: size.height))
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return image!
        }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if UIDevice.current.userInterfaceIdiom == .phone {
            profileBackgrond.image = UIImage(named: "profile_background_iPhone")
        }
        
        self.detectOrientation()
        
        /* Round avatar */
        let viewLayer = photoView.layer
        viewLayer.cornerRadius = viewLayer.frame.size.width / 2
        viewLayer.shadowColor = UIColor.black.cgColor
        viewLayer.shadowOffset = CGSize(width: 0, height: 6)
        viewLayer.shadowOpacity = 0.4
        viewLayer.shadowRadius = 2
        
        let photoLayer = photo.layer
        photoLayer.cornerRadius = photo.frame.size.width / 2
        photo.clipsToBounds = true
        photoLayer.borderWidth = 4.0
        photoLayer.borderColor = UIColor.white.cgColor
        /* ----------- */
    }
    
    // MARK: UI methods
    
    func makeUI() {
        
        textFieldFactory(20, field: nameField, placeholder: "My name")
        textFieldFactory(115, field: emailField, placeholder: "@email")
        
        let tintColor = UIColor(red: 53/255.0, green: 63/255.0, blue: 68/255.0, alpha: 1.0)
        let placeholderColor = UIColor(red: 53/255, green: 63/255, blue: 68/255, alpha: 0.3)
        let textColor = UIColor(red: 60/255, green: 70/255, blue: 75/255, alpha: 1.0)
        aboutField.autocorrectionType = .no
        aboutField.textContainerInset = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
        aboutField.tintColor = tintColor
        
        if aboutField.text == "" || aboutField.text == nil {
            aboutField.text = "I love cars..."
            aboutField.textColor = placeholderColor
        } else {
            aboutField.textColor = textColor
        }
    }
    
    func detectOrientation() {
        if UIDevice.current.userInterfaceIdiom == .pad {
            if (UIDevice.current.orientation.isLandscape){
                self.profileBackgroundTopConstraint?.constant = -800
            } else {
                self.profileBackgroundTopConstraint?.constant = -610
            }
        }
    }
    
    func completeUserData () {
        print("completing user data")
        
        aboutField.text = User.UserData.about
        nameField.text = User.UserData.firstName
        emailField.text = User.UserData.email
        email = emailField.text
        name = nameField.text
        about = aboutField.text
    }
    
    func completeImage() {
        if let imageString = User.UserData.base64String {
            photo.image = convertBase64ToImage(imageString)
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: nil) { (UIViewControllerTransitionCoordinatorContext) in
            self.detectOrientation()
        }
    }
    
    // MARK: Image picker
    
    @IBAction func pickImage(_ sender: AnyObject) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.pickAnImageFromAlbum()
        }))
        actionSheet.addAction(UIAlertAction(title: "Take Photo", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.pickAnImageFromCamera()
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        actionSheet.popoverPresentationController?.sourceView = self.view
        
//        let actionSheetRect = CGRect(
//            origin: CGPoint(x: self.view.bounds.size.width/2, y: self.view.bounds.size.height/2),
//            size: UIScreen.mainScreen().bounds.size
//        )
        
        actionSheet.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width/2, y: 172, width: 10, height: 10)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func pickAnImageFromAlbum() {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    func pickAnImageFromCamera() {
        imagePicker.sourceType = UIImagePickerControllerSourceType.camera
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController( _ picker: UIImagePickerController,
                                didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let imageData = UIImageJPEGRepresentation(pickedImage, 0.0)
            let base64String = imageData!.base64EncodedString(options: Data.Base64EncodingOptions([]))
            
            let reference = FirebaseClient.Constants.UsersURL.child(uid!).child("base64String")
            FirebaseClient.sharedInstance().sendData(reference, data: base64String as AnyObject?, completionHandler: { (success) in
                if success {
                    self.photo.image = pickedImage
                    User.UserData.base64String = base64String
                    self.view.makeToast(self.savedMessage)
                } else {
                    self.view.makeToast(self.errorMessage)
                }
            })
        }
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: TextView
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            let reference = FirebaseClient.Constants.UsersURL.child(uid!).child("about")
            if textView.text != "I love board games..." {
                
                FirebaseClient.sharedInstance().sendData(reference, data: aboutField.text as AnyObject?, completionHandler: { (success) in
                    if success {
                        self.view.makeToast(self.savedMessage)
                    } else {
                        self.view.makeToast(self.errorMessage)
                        self.aboutField.text = self.about
                    }
                })
            } else {
                FirebaseClient.sharedInstance().sendData(reference, data: "" as AnyObject?, completionHandler: { (success) in
                    if success {
                        self.view.makeToast(self.savedMessage)
                    } else {
                        self.view.makeToast(self.errorMessage)
                        self.aboutField.text = self.about
                        self.aboutField.textColor = UIColor(red: 60/255, green: 70/255, blue: 75/255, alpha: 1.0)
                    }
                })
            }
            return false
        }
        let maxtext: Int = 200
        return textView.text.characters.count + (text.characters.count - range.length) <= maxtext
        }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "I love cars..." {
            textView.text = ""
            textView.textColor = UIColor(red: 60/255, green: 70/255, blue: 75/255, alpha: 1.0)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "I love cars..."
            textView.textColor = UIColor(red: 53/255, green: 63/255, blue: 68/255, alpha: 0.3)
        }
    }

    
    // MARK: TextFields
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if nameField.resignFirstResponder() {
            if nameField.text?.characters.count > 3 && nameField.text != ""{
                
                let reference = FirebaseClient.Constants.UsersURL.child(uid!).child("firstName")
                
                FirebaseClient.sharedInstance().sendData(reference, data: nameField.text as AnyObject?, completionHandler: { (success) in
                    if success {
                        User.UserData.firstName = self.nameField.text
                        self.view.makeToast(self.savedMessage)
                    } else {
                        self.nameField.text = self.name
                        self.view.makeToast(self.errorMessage)
                    }
                })
            } else {
                self.showAlert("Please enter a valid name")
                self.nameField.text = name
            }
        } else if emailField.resignFirstResponder() {
            if emailField.text != "" {
                
                if User.UserData.provider != "facebook" {
                    showFieldAlert("", message: "Please enter your password to change email address", secure: true, completionHandler: { (string, success) in
                        self.spinner.startAnimating()
                        if success == true {
                            FirebaseClient.sharedInstance().changeEmail(string, oldEmail: self.email!, newEmail: self.emailField.text!, completionHandler: { (success, message) in
                                self.spinner.stopAnimating()
                                
                                if success {
                                    self.view.makeToast(self.savedMessage)
                                    self.email = self.emailField.text
                                } else {
                                    self.showAlert(message)
                                    self.emailField.text = self.email
                                }
                            })
                        } else {
                            self.emailField.text = self.email
                            self.spinner.stopAnimating()
                        }
                    })
                } else {
                    let reference = FirebaseClient.Constants.UsersURL.child(uid!).child("email")
                    FirebaseClient.sharedInstance().sendData(reference, data: emailField.text as AnyObject?, completionHandler: { (success) in
                        if success {
                            self.view.makeToast(self.savedMessage)
                        } else {
                            self.emailField.text = self.email
                            self.view.makeToast(self.errorMessage)
                        }
                    })
                }
            } else {
                self.showAlert("This field can't be empty")
                self.emailField.text = self.email
            }
        }
        return true
    }
    
    // MARK: TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyProfileCell", for: indexPath)

        cell.textLabel!.font = UIFont.systemFont(ofSize: 17, weight: UIFontWeightLight)
        cell.textLabel?.textColor = UIColor(red: 60/255, green: 70/255, blue: 75/255, alpha: 1.0)
        cell.textLabel?.text = "Change password"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let locationVC = storyboard!.instantiateViewController(withIdentifier: "PasswordVC") as! PasswordVC
        locationVC.title = "Change password"
        navigationController!.pushViewController(locationVC, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: actions
    
    @IBAction func showPrivacy(_ sender: AnyObject) {
        UIApplication.shared.openURL(URL(string: "http://www.leftlane.pl/carspotter/index.html#polityka")!)
    }
    
    @IBAction func logout(_ sender: AnyObject) {
        
        DispatchQueue.main.async(execute: {
            
//            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Game")
//            do {
//                let fetchedEntities = try self.sharedContext.fetch(fetchRequest) as! [Game]
//                for entity in fetchedEntities {
//                    self.sharedContext.delete(entity)
//                }
//            } catch {
//            }
//            do {
//                try self.sharedContext.save()
//            } catch {
//            }
            
            let firebaseAuth = FIRAuth.auth()
            do {
                try firebaseAuth?.signOut()
            } catch let signOutError as NSError {
                print ("Error signing out: %@", signOutError)
            }
            
            FirebaseClient.sharedInstance().fbLogout()
            
            UserDefaults.standard.setValue(nil, forKey: "uid")
            UserDefaults.standard.set(0.0, forKey: "Saved Longitude Span")
            UserDefaults.standard.set(0.0, forKey: "Saved Latitude Span")
            UserDefaults.standard.set(0.0, forKey: "Saved Longitude")
            UserDefaults.standard.set(0.0, forKey: "Saved Latitude")
            User.UserData.longitude = nil
            User.UserData.latitude = nil
            User.UserData.about = nil
            User.UserData.notifications = nil
            User.UserData.privacyAgreement = nil
            
            let reference = FirebaseClient.Constants.UsersURL.child(self.uid!).child("oneSignalID")
            OneSignal.idsAvailable({ (userId, pushToken) in
                if (pushToken != nil) {
                    reference.setValue("")
                }
            })
            
            let controller = self.storyboard!.instantiateViewController(withIdentifier: "LoginVC")
            controller.modalTransitionStyle = UIModalTransitionStyle.flipHorizontal
            self.present(controller, animated: true, completion: nil)
        })
    }
}
