//
//  PasswordVC.swift
//  Carspotter
//
//  Created by Michał Tubis on 12.05.2016.
//  Copyright © 2016 Michał Tubis. All rights reserved.
//

import UIKit
import MapKit
import Crashlytics

class PasswordVC: BaseVC {
    
    @IBOutlet var oldTextField: UITextField!
    @IBOutlet var newTextField: UITextField!
    @IBOutlet var repeatTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Answers.logContentView(withName: "PasswordVC",
                                       contentType: "",
                                       contentId: "",
                                       customAttributes: [:])
        
        let buttonDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont.systemFont(ofSize: 17, weight: UIFontWeightRegular)]
        let cancelButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.save, target: self, action: #selector(PasswordVC.changePassword))
        navigationItem.rightBarButtonItem = cancelButton
        navigationItem.rightBarButtonItem?.setTitleTextAttributes(buttonDict as? [String : AnyObject], for: UIControlState())
        
        spinner.hidesWhenStopped = true
        spinner.center = view.center
        spinner.color = UIColor.black
        view.addSubview(spinner)
        
        oldTextField.delegate = self
        newTextField.delegate = self
        repeatTextField.delegate = self
        oldTextField.becomeFirstResponder()
        textFieldFactory(20, field: oldTextField, placeholder: "Old Password")
        textFieldFactory(20, field: newTextField, placeholder: "New Password")
        textFieldFactory(20, field: repeatTextField, placeholder:"Confirm New Password")
    }
    
    // MARK: Keyboard
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if oldTextField.isFirstResponder {
            oldTextField.resignFirstResponder()
            newTextField.becomeFirstResponder()
        } else if newTextField.isFirstResponder {
            newTextField.resignFirstResponder()
            repeatTextField.becomeFirstResponder()
        } else if repeatTextField.isFirstResponder {
            repeatTextField.resignFirstResponder()
            changePassword()
        }
        return true
    }
    
    func changePassword() {
        spinner.startAnimating()
        if !(oldTextField.text == "" || newTextField.text == "" || repeatTextField.text == "") {
            if newTextField.text! == repeatTextField.text! {
                FirebaseClient.sharedInstance().changePassword(oldTextField.text!, newPassword: newTextField.text!,
                                                               completionHandler: { (success, message) in
                                                                if success {
                                                                    self.view.makeToast(message)
                                                                    self.spinner.stopAnimating()
                                                                    self.view.endEditing(true)
                                                                } else {
                                                                    if message == "Incorrect Password" {
                                                                        self.showAlert("Old password is incorrect. Try again.")
                                                                    } else {
                                                                        self.showAlert(message)
                                                                    }
                                                                    self.spinner.stopAnimating()
                                                                    
                                                                }
                })
            } else {
                showAlert("Your new password and confirmation password are different. Try again.")
                self.spinner.stopAnimating()
            }
        } else {
            showAlert("Some fields seem to be empty!")
            self.spinner.stopAnimating()
        }
    }
    
}
