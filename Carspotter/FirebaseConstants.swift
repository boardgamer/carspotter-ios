//
//  FirebaseConstants.swift
//  Carspotter
//
//  Created by Michał Tubis on 26.04.2016.
//  Copyright © 2016 Michał Tubis. All rights reserved.
//

import Firebase

extension FirebaseClient {

    
    struct Constants {
        
        static var BaseURL = FIRDatabase.database().reference()
        static let UsersURL = FirebaseClient.Constants.BaseURL.child("users/")
        static let MessagesURL = FirebaseClient.Constants.BaseURL.child("messages/")
        static let GamesURL = FirebaseClient.Constants.BaseURL.child("games/")
        static let LocationsURL = FirebaseClient.Constants.BaseURL.child("locations/")

    }
}
