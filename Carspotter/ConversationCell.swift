//
//  ConversationCell.swift
//  Carspotter
//
//  Created by Michał Tubis on 10.06.2016.
//  Copyright © 2016 Michał Tubis. All rights reserved.
//

import UIKit

class ConversationCell: UITableViewCell {

    @IBOutlet var receiverImage: UIImageView!
    @IBOutlet var receiverDisplayName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
