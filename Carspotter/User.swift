//
//  User.swift
//  Carspotter
//
//  Created by Michał Tubis on 26.04.2016.
//  Copyright © 2016 Michał Tubis. All rights reserved.
//

class User {
    
    struct UserData {
        static var firstName: String?
        static var email: String?
        static var latitude: Double?
        static var longitude: Double?
        static var about: String?
        static var base64String: String?
        static var provider: String?
        static var notifications: Int?
        static var oneSignalID: String?
        static var privacyAgreement: Bool?
        static var blockedUsers: [String: AnyObject]?
    }
}
