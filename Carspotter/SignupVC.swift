//
//  SignupVC.swift
//  Carspotter
//
//  Created by Michał Tubis on 12.04.2016.
//  Copyright © 2016 Michał Tubis. All rights reserved.
//

import UIKit
import Crashlytics

class SignupVC: BaseLoginVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Answers.logContentView(withName: "SignupVC",
                                       contentType: "",
                                       contentId: "",
                                       customAttributes: [:])
        
        nameTextField.delegate = self
        emailTextField.delegate = self
        passwordTextField.delegate = self
        
        // Fields and buttons
        textFieldFactory(nameTextField, name: "Name", frame: CGRect(x: 245, y: 13, width: 14.1, height: 18.5))
        textFieldFactory(emailTextField, name: "Email", frame: CGRect(x: 240, y: 13, width: 23.5, height: 18.5))
        textFieldFactory(passwordTextField, name: "Password", frame: CGRect(x: 244, y: 13.5, width: 15, height: 18.5))
        buttonFactory(baseButton, label: "Sign me up!")
    }
    
    @IBAction func signUp(_ sender: AnyObject) {
        signUp()
    }
    
    func signUp() {
        if ((emailTextField.text!.isEmpty) || (passwordTextField.text!.isEmpty || nameTextField.text!.isEmpty)) {
            showAlert("Some fields seem empty")
        } else {
            let name = nameTextField.text!
            let email = emailTextField.text!
            let password = passwordTextField.text!
            spinner.isHidden = false
            spinner.startAnimating()
            FirebaseClient.sharedInstance().createUser(name, email : email, password : password, completionHandler: closureForSignUpDidSucceed)
        }
    }
    
    func closureForSignUpDidSucceed(_ success: Bool, message: String) -> Void {
        if success {
            completeSignUp()
            print(message)
        } else {
            self.spinner.stopAnimating()
            showAlert(message)
        }
    }
    
    func completeSignUp() {
        DispatchQueue.main.async(execute: {
                let controller = self.storyboard!.instantiateViewController(withIdentifier: "TabBarVC")
                self.spinner.stopAnimating()
                self.present(controller, animated: true, completion: nil)
        })
    }
    
    //MARK: Keyboard
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        nameTextField.resignFirstResponder()
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if nameTextField.isFirstResponder {
            nameTextField.resignFirstResponder()
            emailTextField.becomeFirstResponder()
        } else if emailTextField.isFirstResponder {
            emailTextField.resignFirstResponder()
            passwordTextField.becomeFirstResponder()
        } else if passwordTextField.isFirstResponder {
            passwordTextField.resignFirstResponder()
            signUp()
        }
        return true
    }
}

