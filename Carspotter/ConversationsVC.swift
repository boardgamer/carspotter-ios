//
//  ConversationsVC.swift
//  Carspotter
//
//  Created by Michał Tubis on 27.04.2016.
//  Copyright © 2016 Michał Tubis. All rights reserved.
//

import UIKit
import Firebase
import Crashlytics

class ConversationsVC: BaseVC, UITableViewDelegate, UITableViewDataSource {
    
    var conversations = [Conversation]()
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var noLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Answers.logContentView(withName: "ConversationsVC",
                                       contentType: "",
                                       contentId: "",
                                       customAttributes: [:])
        
        tableView.delegate = self
        tableView.dataSource = self
        noLabel.isHidden = true
        
        spinner.hidesWhenStopped = true
        spinner.center = view.center
        spinner.color = UIColor.black
        view.addSubview(spinner)
        
        title = "Conversations"
        navigationController!.navigationBar.barStyle = UIBarStyle.black
        navigationController!.navigationBar.tintColor = UIColor.white
        navigationItem.backBarButtonItem = UIBarButtonItem(title:" ", style:.plain, target:nil, action:nil)
        
        downloadConversations()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    func downloadConversations () {
        
        let uid = UserDefaults.standard.value(forKey: "uid") as! String
        spinner.startAnimating()
        FirebaseClient.Constants.UsersURL.child(uid).child("messages").observe(.value, with: {
            snapshot in
            
            self.conversations = [Conversation]()
            
            if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
                for snap in snapshots {
                    if let dict = snap.value as? Dictionary<String, AnyObject> {
                        if (snap.value as AnyObject).object(forKey: "receiverId") != nil {
                            let key = snap.key
                            let conversation = Conversation(key: key, dictionary: dict)
                            self.conversations.insert(conversation, at: 0)
                            self.conversations.sort(by: {$1.date < $0.date })
                        }
                    }
                }
            }
            self.tableView.reloadData()
            self.spinner.stopAnimating()
            if self.conversations.count == 0 {
                self.tableView.isHidden = true
                self.noLabel.isHidden = false
            } else {
                self.tableView.isHidden = false
                self.noLabel.isHidden = true
            }
        })
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return conversations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        tableView.register(UINib(nibName: "ConversationCell", bundle: nil), forCellReuseIdentifier: "ConversationCell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConversationCell", for: indexPath) as! ConversationCell
        cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        let conversation = conversations[indexPath.row]
        
        
        // Receiver Display Name
        
        cell.receiverDisplayName.text = conversation.displayName
        
        let newConversation: NSMutableAttributedString = NSMutableAttributedString(string: conversation.displayName)
        newConversation.addAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: 17, weight: UIFontWeightBold)], range: NSMakeRange(0, newConversation.length))
        
        let oldConversation: NSMutableAttributedString = NSMutableAttributedString(string: conversation.displayName)
        oldConversation.addAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: 17, weight: UIFontWeightLight)], range: NSMakeRange(0, oldConversation.length))
        
        if conversation.new == true {
            cell.receiverDisplayName?.attributedText = newConversation
        } else {
            cell.receiverDisplayName?.attributedText = oldConversation
        }
        
        
        // Image
        
        cell.receiverImage.image = UIImage(named: "photo_placeholder")
        
//        let qos = Int(DispatchQoS.QoSClass.userInteractive.rawValue)
        DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async(execute: { () -> Void in
            
            //TODO: Ten kod już jest używany w MapVC - reuse
            let image = self.convertBase64ToImage(conversation.base64String)
            var square = CGSize(width: 1000, height: 1000)
            let size = CGSize(width: 55, height: 55)
            
            if image.size.width > 1280 {
                square = CGSize(width: 4000, height: 4000)
            }
            
            let squareImage = image.imageByCroppingImage(square)
            let resizedImage = self.imageResize(squareImage, sizeChange: size)
            
            DispatchQueue.main.async(execute: {
                cell.receiverImage.image = resizedImage
                });
        })
        
        let viewLayer = cell.receiverImage!.layer
        viewLayer.cornerRadius = viewLayer.frame.size.width / 2
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let conversation = conversations[indexPath.row]
        let conversationId = conversation.id
        
        let chatVC = storyboard!.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
        chatVC.senderId = UserDefaults.standard.value(forKey: "uid") as! String
        chatVC.senderDisplayName = conversation.displayName
        chatVC.title = conversation.displayName.components(separatedBy: " ").first
        chatVC.messagesQuery = (FirebaseClient.Constants.MessagesURL.child(conversationId).queryLimited(toLast: 100))
        chatVC.itemId = FirebaseClient.Constants.MessagesURL.child(conversationId)
        chatVC.receiverId = conversation.receiverId
        chatVC.conversationId = conversation.id
        chatVC.imageString = conversation.base64String
        chatVC.hidesBottomBarWhenPushed = true
        navigationController!.pushViewController(chatVC, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            let conversationToDelete = conversations[indexPath.row]
            
            // Delete from Firebase
            let uid = UserDefaults.standard.value(forKey: "uid") as! String
            let reference = FirebaseClient.Constants.UsersURL.child(uid).child("messages").child(conversationToDelete.id)
            FirebaseClient.sharedInstance().sendData(reference, data: nil, completionHandler: { (success) in
                if success {
                    // Delete from games dictionary
                    self.conversations.remove(at: indexPath.row)
                    tableView.deleteRows(at: [indexPath], with: .fade)
                } else {
                    self.view.makeToast("Connection error")
                }
            })
        }
    }
}
