//
//  ChatVC.swift
//  Carspotter
//
//  Created by Michał Tubis on 27.04.2016.
//  Copyright © 2016 Michał Tubis. All rights reserved.
//

import UIKit
import Firebase
import JSQMessagesViewController
import IQKeyboardManagerSwift
import Crashlytics
import OneSignal

class ChatVC: JSQMessagesViewController {
    
    let spinner = UIActivityIndicatorView()
    
    var messages = [JSQMessage]()
    var messagesQuery = FIRDatabaseQuery()
    var itemId = FIRDatabaseReference()
    var receiverId = String()
    var oneSignalID = String()
    var conversationId = String()
    var imageString = String()
    var outgoingBubbleImageView: JSQMessagesBubbleImage!
    var incomingBubbleImageView: JSQMessagesBubbleImage!
    let myUid = UserDefaults.standard.value(forKey: "uid") as! String
    var blocked = false
    var blockLabel = UILabel()
    
    var userIsTypingRef: FIRDatabaseReference!
    var userTypingQuery: FIRDatabaseQuery!
    fileprivate var localTyping = false
    var isTyping: Bool {
        get {
            return localTyping
        }
        set {
            localTyping = newValue
            userIsTypingRef.setValue(newValue)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Answers.logContentView(withName: "ChatVC",
                                       contentType: "",
                                       contentId: "",
                                       customAttributes: [:])
        
        spinner.hidesWhenStopped = true
        spinner.center = CGPoint(x: (UIScreen.main.bounds.width / 2), y: (self.view.frame.height / 2) - (spinner.frame.height / 2))
        spinner.color = UIColor.black
        view.addSubview(spinner)
        
        setupBubbles()
        collectionView!.collectionViewLayout.incomingAvatarViewSize = CGSize.zero
        collectionView!.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
        inputToolbar.contentView.leftBarButtonItem = nil

        navigationItem.backBarButtonItem = UIBarButtonItem(title:" ", style:.plain, target:nil, action:nil)
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Profile", style: .plain, target: self, action: #selector(seeProfile))
        
        blockLabel = UILabel(frame: CGRect(x: 20, y: 144, width: (UIScreen.main.bounds.width) - 40, height: 42))
        blockLabel.textAlignment = NSTextAlignment.center
        blockLabel.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 0.5)
        blockLabel.numberOfLines = 0
        let blockLabelString: NSAttributedString = NSAttributedString(string: "You've blocked \(senderDisplayName). Go to user's Profile to unblock.", attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 13, weight: UIFontWeightLight)])
        blockLabel.attributedText = blockLabelString
        view.addSubview(blockLabel)
        
        observeMessages()
        observeBlocking()
        observeTyping()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        collectionView.isHidden = false
        inputToolbar.isHidden = false
        view.backgroundColor = UIColor.white
        blockLabel.isHidden = true
        
        if User.UserData.blockedUsers != nil {
            for (_, user) in User.UserData.blockedUsers! {
                if user as! String == receiverId {
                    collectionView.isHidden = true
                    inputToolbar.isHidden = true
                    view.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 244/255, alpha: 1.0)
                    blockLabel.isHidden = false
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        finishReceivingMessage()

        IQKeyboardManager.sharedManager().enable = false
        let myUid = UserDefaults.standard.value(forKey: "uid") as! String
        let new = messageRef(myUid)
        
        new.observe(.value, with: { snap in
            
            if snap.value is NSNull {
            } else {
                
                let reference = new.child("new")
                
                FirebaseClient.sharedInstance().sendData(reference, data: false as AnyObject?, completionHandler: { (success) in
                    if success == false {
                        self.view.makeToast("Connection error")
                    }
                })
            }
        })
        
        let oneSignalReferenceID = FirebaseClient.Constants.UsersURL.child(receiverId)
        oneSignalReferenceID.observe(.value, with: { snapshot in
            self.oneSignalID = ((snapshot.value as AnyObject).object(forKey: "oneSignalID") as! String)
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.inputToolbar.contentView.textView.resignFirstResponder()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        let myUid = UserDefaults.standard.value(forKey: "uid") as! String
        let new = messageRef(myUid)
        let oneSignalReferenceID = FirebaseClient.Constants.UsersURL.child(receiverId)
        
        new.removeAllObservers()
        isTyping = false
        oneSignalReferenceID.removeAllObservers()
        IQKeyboardManager.sharedManager().enable = true
    }
    
    func seeProfile() {
        
        spinner.startAnimating()
        FirebaseClient.sharedInstance().getPublicUserData(receiverId) { (result, success) in
            
            let userProfile = self.storyboard!.instantiateViewController(withIdentifier: "UserProfileVC") as! UserProfileVC
            
            userProfile.uid = self.receiverId
            userProfile.displayName = (result["displayName"] as? String)!
            userProfile.about = (result["about"] as? String)!
            userProfile.imageString = (result["base64String"] as? String)!
            userProfile.latitude = (result["latitude"] as? Double)
            userProfile.longitude = (result["longitude"] as? Double)
            userProfile.showMessage = false
            userProfile.blocking = self.blocked
            self.navigationController!.pushViewController(userProfile, animated: true)
            self.spinner.stopAnimating()
        }
    }
    
    // MARK: JSQMessage Delegate
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = messages[indexPath.item]
        if message.senderId == senderId {
            return outgoingBubbleImageView
        } else {
            return incomingBubbleImageView
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        let message = messages[indexPath.item]
        
        if message.senderId == senderId {
            cell.textView.textColor = UIColor.white
//            cell.cellBottomLabel.text = "Delivered"
        } else {
            cell.textView.textColor = UIColor.black
        }
        return cell
    }
    
//    override func collectionView(collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellBottomLabelAtIndexPath indexPath: NSIndexPath!) -> CGFloat {
//        return CGFloat(20)
//    }
    
    override func textViewDidChange(_ textView: UITextView) {
        super.textViewDidChange(textView)
        // If the text is not empty, the user is typing
        isTyping = textView.text != ""
    }
    
    //MARK: Messages
    
    fileprivate func setupBubbles() {
        let factory = JSQMessagesBubbleImageFactory()
        outgoingBubbleImageView = factory?.outgoingMessagesBubbleImage(with: UIColor(red: 211/255, green: 111/255, blue: 5/255, alpha: 1.0))
        incomingBubbleImageView = factory?.incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleLightGray())
    }
    
    func addMessage(_ id: String, text: String){
        let message = JSQMessage(senderId: id, displayName: "", text: text)
        messages.append(message!)
    }
    
    fileprivate func observeMessages() {
        print("observe messages")

        messagesQuery.observe(.childAdded) { (snapshot: FIRDataSnapshot?) in
            
            let snapshotValue = snapshot?.value as? NSDictionary
            let id = snapshotValue?["senderId"] as! String
            let text = snapshotValue?["text"] as! String
            
            self.addMessage(id, text: text)
            self.finishReceivingMessage()
        }
    }
    
    func observeBlocking() {
        let blockedUsersReference = FirebaseClient.Constants.UsersURL.child(receiverId).child("blockedUsers")
        blockedUsersReference.observe(.value, with: { snapshot in
            
            if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
                for snap in snapshots {
                    if snap.value as! String == self.myUid {
                        print("I'm blocked!!!")
                        self.blocked = true
                    }
                }
            }
        })
    }
    
    fileprivate func observeTyping() {

        let typingIndicatorRef = messageRef(receiverId).child("typingIndicator")
        userIsTypingRef = typingIndicatorRef
        userIsTypingRef.onDisconnectRemoveValue()
        
        userTypingQuery = messageRef(myUid).child("typingIndicator")
        
        userTypingQuery.observe(.value) { (data: FIRDataSnapshot?) in
            
            if data?.value as? Bool == true {
                print("Someone's typing!")
                self.showTypingIndicator = true
                self.scrollToBottom(animated: true)
            } else {
                print("Noone's typing")
                self.showTypingIndicator = false
                self.scrollToBottom(animated: true)
            }
        }
    }
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateString = dateFormatter.string(from:date)
        let messageItem = [
            "text": text,
            "senderId": senderId,
            "receiverId": receiverId
        ]
        
        let myReference = messageRef(myUid)
        let myMessageInfo = [
            "id": conversationId,
            "receiverId": receiverId,
            "displayName": senderDisplayName,
            "base64String": imageString,
            "oneSignalID": oneSignalID,
            "new": false,
            "date": dateString
        ] as [String : Any]
        
        let receiverReference = messageRef(receiverId)
        let receiverMessageInfo = [
            "id": conversationId,
            "receiverId": myUid,
            "displayName": User.UserData.firstName!,
            "base64String": User.UserData.base64String!,
            "oneSignalID": User.UserData.oneSignalID!,
            "new": true,
            "date": dateString
        ] as [String : Any]
        
        if blocked == false {
            
            OneSignal.postNotification(["contents": ["en": "\(User.UserData.firstName!): " + text], "include_player_ids": [oneSignalID], "ios_badgeType": "Increase", "ios_badgeCount": 1])
            
            FirebaseClient.sharedInstance().sendData(myReference, data: myMessageInfo as AnyObject?) { (success) in
                if success {
                    FirebaseClient.sharedInstance().sendData(receiverReference, data: receiverMessageInfo as AnyObject?) { (success) in
                        if success {
                            FirebaseClient.sharedInstance().sendData(self.itemId.childByAutoId(), data: messageItem as AnyObject?, completionHandler: { (success) in
                                if success {
                                    JSQSystemSoundPlayer.jsq_playMessageSentSound()
                                    self.finishSendingMessage()
                                } else {
                                    self.inputToolbar.contentView.textView.resignFirstResponder()
                                    self.view.makeToast("Connection error")
                                }
                            })
                        } else {
                            self.inputToolbar.contentView.textView.resignFirstResponder()
                            self.view.makeToast("Connection error")
                        }
                    }
                } else {
                    self.inputToolbar.contentView.textView.resignFirstResponder()
                    self.view.makeToast("Connection error")
                }
            }
            
        } else {
            print("You're blocked, dude...")
            
            FirebaseClient.sharedInstance().sendData(self.itemId.childByAutoId(), data: messageItem as AnyObject?, completionHandler: { (success) in
                if success {
                    JSQSystemSoundPlayer.jsq_playMessageSentSound()
                    self.finishSendingMessage()
                } else {
                    self.inputToolbar.contentView.textView.resignFirstResponder()
                    self.view.makeToast("Connection error")
                }
            })
        }
    }
    
    //MARK: Helpers
    
    func messageRef(_ id: String) -> (FIRDatabaseReference) {
        let reference = FirebaseClient.Constants.UsersURL.child(id).child("messages").child(conversationId)
        
        return reference
    }
}
