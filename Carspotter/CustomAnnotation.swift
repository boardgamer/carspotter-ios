//
//  CustomAnnotation.swift
//  Carspotter
//
//  Created by Michał Tubis on 17.05.2016.
//  Copyright © 2016 Michał Tubis. All rights reserved.
//

import UIKit
import MapKit

class CustomPointAnnotation: MKPointAnnotation {
    
    var imageString: String!
    var displayName: String!
    var about: String!
    var uid: String!
    var latitude: Double!
    var longitude: Double!
    var blockedUsers: [String: AnyObject]?
    var blocked: Bool?
}
