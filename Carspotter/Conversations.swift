//
//  Conversations.swift
//  Carspotter
//
//  Created by Michał Tubis on 19.05.2016.
//  Copyright © 2016 Michał Tubis. All rights reserved.
//

import Firebase

class Conversation {
    
    fileprivate var _key: String!
    fileprivate var _id: String!
    fileprivate var _displayName: String!
    fileprivate var _receiverId: String!
    fileprivate var _oneSignalID: String!
    fileprivate var _base64String: String!
    fileprivate var _new: Bool!
    fileprivate var _date: String = ""
    
    var key: String {
        return _key
    }
    
    var id: String {
        return _id
    }
    
    var displayName: String {
        return _displayName
    }
    
    var receiverId: String {
        return _receiverId
    }
    
    var oneSignalID: String {
        return _oneSignalID
    }
    
    var base64String: String {
        return _base64String
    }
    
    var new: Bool {
        return _new
    }
    
    var date: String {
        return _date
    }
    
    // Initialize new Conversation
    
    init(key: String, dictionary: Dictionary<String, AnyObject>) {
        self._key = key
        
        if let id = dictionary["id"] as? String {
            self._id = id
        }
        
        if let displayName = dictionary["displayName"] as? String {
            self._displayName = displayName
        }
        
        if let receiverId = dictionary["receiverId"] as? String {
            self._receiverId = receiverId
        }
        
        if let oneSignalID = dictionary["oneSignalID"] as? String {
            self._oneSignalID = oneSignalID
        }
        
        if let base64String = dictionary["base64String"] as? String {
            self._base64String = base64String
        }
        
        if let new = dictionary["new"] as? Bool {
            self._new = new
        }
        
        if let date = dictionary["date"] as? String{
            self._date = date
        }
    }
    
    var image: UIImage? {
        
        //TODO: implement this in conversations list, check if files are written to memory
        
        get {
            let fileName = base64String.substring(from: base64String.characters.index(base64String.endIndex, offsetBy: -4))

            return ImageCache.Caches.imageCache.imageWithIdentifier(fileName)
        }
        
        set {
            
            let decodedData = NSData(base64Encoded: base64String, options: NSData.Base64DecodingOptions(rawValue: 0) )
            let decodedimage = UIImage(data: decodedData! as Data)
            let image = decodedimage
            var square = CGSize(width: 1000, height: 1000)
            let size = CGSize(width: 55, height: 55)
            if image!.size.width > 1280 {
                square = CGSize(width: 4000, height: 4000)
            }
            let squareImage = image!.imageByCroppingImage(square)
            let resizedImage = self.imageResize(squareImage, sizeChange: size)
            let fileName = base64String.substring(from: base64String.characters.index(base64String.endIndex, offsetBy: -4))

            ImageCache.Caches.imageCache.storeImage(resizedImage, withIdentifier: fileName)
            
        }
    }
    
    func imageResize (_ image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 3.5 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return scaledImage!
    }
}

