//
//  LoginVC.swift
//  Carspotter
//
//  Created by Michał Tubis on 16.04.2016.
//  Copyright © 2016 Michał Tubis. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import Crashlytics

class LoginVC: BaseLoginVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Answers.logContentView(withName: "LoginVC",
                                       contentType: "",
                                       contentId: "",
                                       customAttributes: [:])

        
        emailTextField.delegate = self
        passwordTextField.delegate = self
                
        // Fields and buttons
        textFieldFactory(emailTextField, name: "Email", frame: CGRect(x: 240.0, y: 13, width: 23.5, height: 18.5))
        textFieldFactory(passwordTextField, name: "Password", frame: CGRect(x: 244, y: 13.5, width: 15, height: 18.5))
        buttonFactory(baseButton, label: "Log in")
        buttonFactory(fbButton, label: "")
    }
    
    @IBAction func logIn(_ sender: AnyObject) {
        
        if ((emailTextField.text!.isEmpty) || (passwordTextField.text!.isEmpty)) {
            showAlert("Some fields seem empty")
        } else {
            let email = emailTextField.text!
            let password = passwordTextField.text!
            spinner.isHidden = false
            spinner.startAnimating()
            FirebaseClient.sharedInstance().logInUser(email, password : password, completionHandler: closureForLogInDidSucceed)
        }
    }
    
    func logIn() {
        if ((emailTextField.text!.isEmpty) || (passwordTextField.text!.isEmpty)) {
            showAlert("Some fields seem empty")
        } else {
            let email = emailTextField.text!
            let password = passwordTextField.text!
            spinner.isHidden = false
            spinner.startAnimating()
            FirebaseClient.sharedInstance().logInUser(email, password : password, completionHandler: closureForLogInDidSucceed)
        }
    }
    
    @IBAction func fbAction(_ sender: AnyObject) {
        spinner.startAnimating()
        FirebaseClient.sharedInstance().fbLogIn(closureForLogInDidSucceed)
    }
    
    func closureForLogInDidSucceed(_ success: Bool, message: String) -> Void {
        if success == true {
            self.spinner.stopAnimating()
            completeLogIn()
        } else if message == "Cancelled" {
            self.spinner.stopAnimating()
            self.view.makeToast(message)
        } else {
            self.spinner.stopAnimating()
            showAlert(message)
        }
    }
    
    func completeLogIn() {
        DispatchQueue.main.async(execute: {
            let controller = self.storyboard!.instantiateViewController(withIdentifier: "TabBarVC")
            self.spinner.stopAnimating()
            print("opening tabbar")
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @IBAction func showSignUp(_ sender: AnyObject) {
        showView("SignUpVC")
    }
    
    @IBAction func showReset(_ sender: AnyObject) {
        
        spinner.startAnimating()
        showFieldAlert("Forgot password?", message: "Please enter your email address below and we'll send you instructions on how to reset your password", secure: false) { (string, success) in
            
            if success == true {
                FirebaseClient.sharedInstance().resetPassword(string, completionHandler: {success, message in
                    self.spinner.stopAnimating()
                    self.showAlert(message)
                })
            } else {
                self.spinner.stopAnimating()
            }
        }
    }
    
    //MARK: Keyboard
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if emailTextField.isFirstResponder {
            emailTextField.resignFirstResponder()
            passwordTextField.becomeFirstResponder()
        } else if passwordTextField.isFirstResponder {
            passwordTextField.resignFirstResponder()
            logIn()
        }
        return true
    }
    
}
