//
//  UserProfileVC.swift
//  Carspotter
//
//  Created by Michał Tubis on 18.05.2016.
//  Copyright © 2016 Michał Tubis. All rights reserved.
//

import UIKit
import MapKit
import Firebase
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}

import Crashlytics

class UserProfileVC: BaseVC, MKMapViewDelegate, UINavigationControllerDelegate {
    
    @IBOutlet var photoView: UIView!
    @IBOutlet var photo: UIImageView!
    @IBOutlet var messageButton: UIButton!
    @IBOutlet var displayNameLabel: UILabel!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var aboutLabel: UITextView!
    @IBOutlet var backgroundImage: UIImageView!
    @IBOutlet var mapView: MKMapView!
    
//    @IBOutlet var backgroundImageHeightConstraint: NSLayoutConstraint!
    
    let blockButton = UIButton()
    let reportButton = UIButton()
    var displayName = String()
    var latitude : CLLocationDegrees?
    var longitude : CLLocationDegrees?
    let annotation = MKPointAnnotation()
    var about = String()
    var imageString = String()
    var uid : String!
    var conversationId : String!
    var showMessage = Bool()
    var blocked = Bool()
    var blocking = Bool()
    var blockedUsers = [String: AnyObject]()
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    // MARK: App Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Answers.logContentView(withName: "UserProfileVC",
                                       contentType: "",
                                       contentId: "",
                                       customAttributes: [:])
        
        mapView.isScrollEnabled = false
        
        if isCheckedIn() {
            annotation.coordinate = CLLocationCoordinate2DMake(latitude!, longitude!)
            mapView.addAnnotation(annotation)
            mapView.setRegion(setRegion(), animated: true)
            mapView.centerCoordinate = annotation.coordinate
        } else {
            mapView.isHidden = true
        }
        
        spinner.hidesWhenStopped = true
        spinner.center = view.center
        spinner.color = UIColor.black
        view.addSubview(spinner)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title:" ", style:.plain, target:nil, action:nil)
        
        blockButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        blockButton.setImage(UIImage(named: "blockUser_button"), for: UIControlState())
        blockButton.addTarget(self, action: #selector(blockUser), for: .touchUpInside)
        
        reportButton.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        reportButton.setImage(UIImage(named: "flag"), for: UIControlState())
        reportButton.addTarget(self, action: #selector(reportUser), for: .touchUpInside)
        
        let blockButtonItem = UIBarButtonItem()
        blockButtonItem.customView = blockButton
        
        let reportButtonItem = UIBarButtonItem()
        reportButtonItem.customView = reportButton
        
        navigationItem.rightBarButtonItems = [blockButtonItem, reportButtonItem]
        
        title = displayName.components(separatedBy: " ").first
        displayNameLabel.text = displayName
        aboutLabel.text = about
        photo.image = convertBase64ToImage(imageString)
        
        let myUid = UserDefaults.standard.value(forKey: "uid") as! String
        if myUid == uid || showMessage == false {
            messageButton.isHidden = true
            blockButton.isHidden = true
            reportButton.isHidden = true
        }

        detectOrientation()
        makeRoundAvatar()
        computeHeaderHeight()
        computeBackgroundSize()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if User.UserData.blockedUsers != nil {
            blockedUsers = User.UserData.blockedUsers!
            print(uid)
        } else {
            self.unblock()
        }
        
        for (_, user) in blockedUsers {
            if user as! String == uid {
                self.block()
            }
        }
        
        if blocking != true {
            if latitude != nil && longitude != nil {
                reverseGeocoding()
            }
        }
    }
    
    // MARK: MapView
    
    func setRegion() -> MKCoordinateRegion {
        let span = MKCoordinateSpanMake(0.1, 0.1)
        let location = CLLocationCoordinate2D(latitude: latitude!, longitude: longitude!)
        let region = MKCoordinateRegionMake(location, span)
        mapView.setRegion(region, animated: false)
        return region
    }
    
    // MARK: UI Elements
    
    func computeBackgroundSize() {
        if UIDevice.current.userInterfaceIdiom == .phone {
//            backgroundImageHeightConstraint.constant = 0
            backgroundImage.image = UIImage(named: "profile_background_iPhone")
        }
    }
    
    func computeHeaderHeight() {
        let aboutString = aboutLabel.text
        let stringLength: Int = (aboutString?.characters.count)!
        let stringFloat = CGFloat(stringLength) * 0.4
        
//        if stringLength < 50 {
//            headerView.frame = CGRect(x: 0, y: 0, width: 600, height: 430)
//        } else {
//            headerView.frame = CGRect(x: 0, y: 0, width: 600, height: 430 + stringFloat)
//        }
    }
    
    func detectOrientation() {
        if UIDevice.current.userInterfaceIdiom == .pad {
//            if (UIDevice.current.orientation.isLandscape){
//                self.backgroundImageHeightConstraint.constant = -800
//            } else {
//                self.backgroundImageHeightConstraint.constant = -610
//            }
        }
    }
    
    func makeRoundAvatar() {
        let viewLayer = photoView.layer
        viewLayer.cornerRadius = viewLayer.frame.size.width / 2
        viewLayer.shadowColor = UIColor.black.cgColor
        viewLayer.shadowOffset = CGSize(width: 0, height: 6)
        viewLayer.shadowOpacity = 0.4
        viewLayer.shadowRadius = 2
        
        let photoLayer = photo.layer
        photoLayer.cornerRadius = photo.frame.size.width / 2
        photo.clipsToBounds = true
        photoLayer.borderWidth = 4.0
        photoLayer.borderColor = UIColor.white.cgColor
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: nil) { (UIViewControllerTransitionCoordinatorContext) in
            self.detectOrientation()
        }
    }
    
    // MARK: Actions
    
    @IBAction func message(_ sender: AnyObject) {
        let myUid = UserDefaults.standard.value(forKey: "uid") as! String

        if uid > myUid {
            conversationId = uid + myUid
        } else {
            conversationId = myUid + uid
        }
        print(conversationId)
        let itemId = FirebaseClient.Constants.MessagesURL.child(conversationId)
        
        let chatVC = self.storyboard!.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
        chatVC.messagesQuery = FirebaseClient.Constants.MessagesURL.child(conversationId).queryLimited(toLast: 100)
        chatVC.senderId = myUid
        chatVC.senderDisplayName = displayName
        chatVC.title = displayName
        chatVC.receiverId = uid
        chatVC.itemId = itemId
        chatVC.conversationId = conversationId
        chatVC.imageString = imageString
        chatVC.hidesBottomBarWhenPushed = true
        
        self.tabBarController?.selectedIndex = 1
        let rootNavigationController = self.tabBarController?.viewControllers![1] as! UINavigationController
        
        rootNavigationController.popToRootViewController(animated: false)
        rootNavigationController.pushViewController(chatVC, animated: true)

    }
    
    func blockUser() {
        
        let myUid = UserDefaults.standard.value(forKey: "uid") as! String
        let reference = FirebaseClient.Constants.LocationsURL.child(myUid).child("blockedUsers").child(uid)
        
        if blocked == false {
            
            showOKCancelAlert("Do you want to block \(displayName)?", message: "\(displayName) won't be able to message you or view your location on the map.", completionHandler: { (success) in
                if success == true {
                    FirebaseClient.sharedInstance().sendData(reference, data: self.uid as AnyObject?) { (success) in
                        if success {
                            self.block()
                            if User.UserData.blockedUsers != nil {
                                User.UserData.blockedUsers![self.uid] = self.uid as AnyObject?
                            } else {
                                let blockedUserData = [self.uid : self.uid]
                                User.UserData.blockedUsers = blockedUserData as [String : AnyObject]?
                            }
                        } else {
                            self.view.makeToast("Connection error")
                        }
                    }
                }
            })
        } else {
            FirebaseClient.sharedInstance().sendData(reference, data: nil, completionHandler: { (success) in
                if success {
                    self.unblock()
                    User.UserData.blockedUsers![self.uid] = nil
                } else {
                    self.view.makeToast("Connection error")
                }
            })
        }
    }
    
    func reportUser() {
        
        let reference = FirebaseClient.Constants.BaseURL.child("reports/").child(uid)
        
        showOKCancelAlert("Do you want to report \(displayName)?", message: "This will inform the admin about \(displayName)'s violation of our terms.") { (success) in
            if success == true {
                FirebaseClient.sharedInstance().sendData(reference, data: self.uid as AnyObject?) { (success) in
                    if success {
                        self.view.makeToast("Thank you!")
                    } else {
                        self.view.makeToast("Connection error")
                    }
                }
            }
        }
    }
    
    // MARK: Helpers
    
    func displayLocationInfo(_ placemark: CLPlacemark) {
        
        if placemark.locality != nil && placemark.subLocality != nil {
            if placemark.locality != placemark.subLocality {
                self.locationLabel.text = placemark.locality! + ", " + placemark.subLocality!
            } else {
                self.locationLabel.text = placemark.locality!
            }
        } else if placemark.locality == nil && placemark.subLocality != nil {
            self.locationLabel.text = placemark.subLocality
        } else if placemark.locality != nil && placemark.subLocality == nil {
            self.locationLabel.text = placemark.locality
        } else {
            self.locationLabel.text = ""
        }
    }
    
    
    func reverseGeocoding() {
        let geocoder = CLGeocoder()
        
        let location: CLLocation =  CLLocation(latitude: latitude!, longitude: longitude!)
        geocoder.reverseGeocodeLocation(location, completionHandler: {(placemarks, error)-> Void in
            if (error != nil) {
                print("Reverse geocoder failed with error" + error!.localizedDescription)
                return
            }
            
            if placemarks!.count > 0 {
                let pm = placemarks![0] as CLPlacemark
                self.displayLocationInfo(pm)
            } else {
                print("Problem with the data received from geocoder")
            }
        })
    }
    
    func block() {
        self.blockButton.setImage(UIImage(named: "unblockUser_button"), for: UIControlState())
        blocked = true
    }
    
    func unblock() {
        blockButton.setImage(UIImage(named: "blockUser_button"), for: UIControlState())
        blocked = false
    }
    
    func isCheckedIn() -> Bool {
        if latitude == nil || longitude == nil {
            return false
        }
        return true
    }
}
