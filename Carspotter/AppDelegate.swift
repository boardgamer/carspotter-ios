//
//  AppDelegate.swift
//  Carspotter
//
//  Created by Michał Tubis on 12.04.2016.
//  Copyright © 2016 Michał Tubis. All rights reserved.
//

import UIKit
import CoreData
import Fabric
import Crashlytics
import IQKeyboardManagerSwift
import FBSDKCoreKit
import FBSDKLoginKit
import Firebase
import OneSignal

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        let plistFileName = Bundle.main.infoDictionary?["TargetName"] as! String
        var googleServiceInfoFilePath = String()
        
        if plistFileName == "Carspotter"{
            print("It's Carspotter")
            googleServiceInfoFilePath = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist")!
            OneSignal.initWithLaunchOptions(launchOptions, appId: "440f2815-7848-41d4-a624-d498b8b42f49", handleNotificationReceived:nil, handleNotificationAction:nil, settings: [kOSSettingsKeyInFocusDisplayOption: "", kOSSettingsKeyAutoPrompt: true])
        }
        if plistFileName == "Carspotter stage"{
            print("It's Carspotter stage")
            googleServiceInfoFilePath = Bundle.main.path(forResource: "GoogleService-Info-stage", ofType: "plist")!
            OneSignal.initWithLaunchOptions(launchOptions, appId: "0eb0fe09-aed4-4800-bc13-1028704b4b8a", handleNotificationReceived:nil, handleNotificationAction:nil, settings: [kOSSettingsKeyInFocusDisplayOption: "", kOSSettingsKeyAutoPrompt: true])
        }
        let firOptions = FIROptions(contentsOfFile: googleServiceInfoFilePath)
        FIRApp.configure(with: firOptions!)
        let notificationTypes: UIUserNotificationType = [UIUserNotificationType.alert, UIUserNotificationType.badge, UIUserNotificationType.sound]
        application.registerUserNotificationSettings(UIUserNotificationSettings(types:
            notificationTypes, categories: nil
            ))
        
        Fabric.sharedSDK().debug = true
        Fabric.with([Crashlytics.self])
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        
        sleep(1)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginVC")
        let tabBarVC = storyboard.instantiateViewController(withIdentifier: "TabBarVC")
        let uid = UserDefaults.standard.value(forKey: "uid") as? String
        
        if FIRAuth.auth()?.currentUser != nil {
            // User is signed in.
            print("User is signed in.")
            
            if uid != nil {
                self.window?.rootViewController = tabBarVC
            } else {
                self.window?.rootViewController = loginVC
            }
            
            // ...
        } else {
            // No user is signed in.
            print("No user is signed in")
            self.window?.rootViewController = loginVC
            // ...
        }

        self.window?.makeKeyAndVisible()

        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {

    }

    func applicationDidEnterBackground(_ application: UIApplication) {

    }

    func applicationWillEnterForeground(_ application: UIApplication) {

    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
    }

    func applicationWillTerminate(_ application: UIApplication) {

    }
}

